#!/usr/bin/env python

# based on:
# https://linux-sunxi.org/LCD#Mainline_Linux_.28simple-panel.29
# https://patchwork.kernel.org/project/linux-fbdev/patch/20160831081817.5191-4-maxime.ripard@free-electrons.com/
# https://www.kernel.org/doc/html/v4.10/media/uapi/v4l/subdev-formats.html

print("Not compatible with LVDS panels! Would need little remake, which shouldn't be hard - you can try it and send me the code!.\n")

cfg = input("Gimme whole CONFIG_VIDEO_LCD_MODE line from your u-boot config: ")
compatible = input("Gimme device tree compatible string: ")
width = input("Gimme screen physical width of panel [mm]: ")
height = input("Gimme screen physical height of panel [mm]: ")

def intify(x):
	return x[0], int(x[1])

cfg = dict(intify(x.split(':')) for x in cfg.split('"')[1].split(','))

print(f"""

Add this to 'drivers/gpu/drm/panel/panel-simple.c' somewhere after 'static void panel_simple_shutdown(struct device *dev)' line:

static const struct drm_display_mode {compatible.replace(',', '_')}_mode = """'{'f"""
	.clock = {cfg['pclk_khz']},
	.hdisplay = {cfg['x']},
	.hsync_start = {cfg['x']} + {cfg['ri']},
	.hsync_end = {cfg['x']} + {cfg['ri']} + {cfg['hs']},
	.htotal = {cfg['x']} + {cfg['ri']} + {cfg['hs']} + {cfg['le']},
	.vdisplay = {cfg['y']},
	.vsync_start = {cfg['y']} + {cfg['lo']},
	.vsync_end = {cfg['y']} + {cfg['lo']} + {cfg['vs']},
	.vtotal = {cfg['y']} + {cfg['lo']} + {cfg['vs']} + {cfg['up']},
"""'};'f"""

static const struct panel_desc {compatible.replace(',', '_')} = """'{'f"""
	.modes = &{compatible.replace(',', '_')}_mode,
	.num_modes = 1,
	.bpc = {cfg['depth']//3},
	.size = """'{'f"""
		.width = {width},
		.height = {height},
	""""""},
	.bus_format = """f'{"MEDIA_BUS_FMT_RGB666_1X18" if cfg["depth"] == 18 else "MEDIA_BUS_FMT_RGB888_1X24"}'""",
	.connector_type = DRM_MODE_CONNECTOR_DPI,
};


And this into 'platform_of_match[]' array:

	"""'{'f"""
		.compatible = "{compatible}",
		.data = &{compatible.replace(',', '_')},
	"""'},'
)
